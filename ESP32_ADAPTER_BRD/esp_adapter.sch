EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L RF_Module:ESP32-WROOM-32 U1
U 1 1 5F24C1DB
P 2300 3100
F 0 "U1" H 2300 4681 50  0000 C CNN
F 1 "ESP32-WROOM-32" H 2300 4590 50  0000 C CNN
F 2 "RF_Module:ESP32-WROOM-32" H 2300 1600 50  0001 C CNN
F 3 "https://www.espressif.com/sites/default/files/documentation/esp32-wroom-32_datasheet_en.pdf" H 2000 3150 50  0001 C CNN
	1    2300 3100
	1    0    0    -1  
$EndComp
Text GLabel 2300 1700 1    50   Input ~ 0
VDD
Text GLabel 2300 4500 3    50   Input ~ 0
GND
Text GLabel 4700 3350 0    50   Input ~ 0
VDD
Text GLabel 4700 4550 0    50   Input ~ 0
GND
Text GLabel 4700 3550 0    50   Input ~ 0
A0
Text GLabel 4700 3650 0    50   Input ~ 0
A1
Text GLabel 4700 3750 0    50   Input ~ 0
A2
Text GLabel 4700 3850 0    50   Input ~ 0
A3
Text GLabel 4700 4050 0    50   Input ~ 0
A5
Text GLabel 4700 4150 0    50   Input ~ 0
A6
Text GLabel 5650 4650 2    50   Input ~ 0
TX
Text GLabel 5650 4550 2    50   Input ~ 0
RX
Text GLabel 5650 4450 2    50   Input ~ 0
RST
Text GLabel 5650 4350 2    50   Input ~ 0
GND
Text GLabel 5650 4250 2    50   Input ~ 0
D2
Text GLabel 5650 4150 2    50   Input ~ 0
D3
Text GLabel 5650 4050 2    50   Input ~ 0
D4
Text GLabel 5650 3950 2    50   Input ~ 0
D5
Text GLabel 5650 3850 2    50   Input ~ 0
D6
Text GLabel 5650 3750 2    50   Input ~ 0
D7
Text GLabel 5650 3650 2    50   Input ~ 0
D8
Text GLabel 5650 3550 2    50   Input ~ 0
D9
Text GLabel 5650 3450 2    50   Input ~ 0
D10
Text GLabel 5650 3350 2    50   Input ~ 0
D11
Text GLabel 2900 1900 2    50   Input ~ 0
A1
Text GLabel 2900 2900 2    50   Input ~ 0
RX
Text GLabel 2900 3000 2    50   Input ~ 0
TX
Text GLabel 2900 2100 2    50   Input ~ 0
A2
Text GLabel 2900 2800 2    50   Input ~ 0
A3
Text GLabel 2900 2300 2    50   Input ~ 0
A0
Text GLabel 1700 2100 0    50   Input ~ 0
A6
Text GLabel 1700 2200 0    50   Input ~ 0
A7
$Comp
L Connector:Conn_01x15_Male J2
U 1 1 5F2A4E46
P 5450 3950
F 0 "J2" H 5558 4831 50  0000 C CNN
F 1 "Conn_01x15_Male" H 5558 4740 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x15_P2.54mm_Vertical" H 5450 3950 50  0001 C CNN
F 3 "~" H 5450 3950 50  0001 C CNN
	1    5450 3950
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x15_Male J1
U 1 1 5F2A6A58
P 4900 3950
F 0 "J1" H 4872 3974 50  0000 R CNN
F 1 "Conn_01x15_Male" H 4872 3883 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x15_P2.54mm_Vertical" H 4900 3950 50  0001 C CNN
F 3 "~" H 4900 3950 50  0001 C CNN
	1    4900 3950
	-1   0    0    -1  
$EndComp
Text GLabel 4700 4250 0    50   Input ~ 0
A7
Text GLabel 5650 3250 2    50   Input ~ 0
D12
Text GLabel 4700 3950 0    50   Input ~ 0
A4
Text GLabel 2900 3700 2    50   Input ~ 0
D2
Text GLabel 2900 3600 2    50   Input ~ 0
D7
Text GLabel 1450 1400 0    50   Input ~ 0
VDD
Text GLabel 2900 3900 2    50   Input ~ 0
A4
Text GLabel 2900 4000 2    50   Input ~ 0
A5
Text GLabel 2900 2500 2    50   Input ~ 0
D3
Text GLabel 2900 3100 2    50   Input ~ 0
D5
Text GLabel 2900 2700 2    50   Input ~ 0
D6
Text GLabel 2900 2400 2    50   Input ~ 0
D9
Text GLabel 1700 3200 0    50   Input ~ 0
D4
Text GLabel 2900 3200 2    50   Input ~ 0
D10
Text GLabel 2900 3800 2    50   Input ~ 0
D8
Text GLabel 4700 3250 0    50   Input ~ 0
D13
Text GLabel 2900 3500 2    50   Input ~ 0
D13
Text GLabel 2900 3300 2    50   Input ~ 0
D11
Text GLabel 2900 3400 2    50   Input ~ 0
D12
$Comp
L Switch:SW_Push SW1
U 1 1 5F21BD44
P 1350 1900
F 0 "SW1" H 1350 2185 50  0000 C CNN
F 1 "SW_Push" H 1350 2094 50  0000 C CNN
F 2 "Button_Switch_SMD:SW_SPST_CK_RS282G05A3" H 1350 2100 50  0001 C CNN
F 3 "~" H 1350 2100 50  0001 C CNN
	1    1350 1900
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x04_Female J3
U 1 1 5F21C2B7
P 6000 1950
F 0 "J3" H 6028 1926 50  0000 L CNN
F 1 "Conn_01x04_Female" H 6028 1835 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 6000 1950 50  0001 C CNN
F 3 "~" H 6000 1950 50  0001 C CNN
	1    6000 1950
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D1
U 1 1 5F21C7A0
P 3350 2600
F 0 "D1" H 3343 2817 50  0000 C CNN
F 1 "LED" H 3343 2726 50  0000 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 3350 2600 50  0001 C CNN
F 3 "~" H 3350 2600 50  0001 C CNN
	1    3350 2600
	-1   0    0    1   
$EndComp
$Comp
L Device:R R2
U 1 1 5F21CB7B
P 3650 2600
F 0 "R2" V 3443 2600 50  0000 C CNN
F 1 "R" V 3534 2600 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3580 2600 50  0001 C CNN
F 3 "~" H 3650 2600 50  0001 C CNN
	1    3650 2600
	0    1    1    0   
$EndComp
Text GLabel 2900 2000 2    50   Input ~ 0
TXDO
Text GLabel 2900 2200 2    50   Input ~ 0
RXDO
Text GLabel 5800 2050 0    50   Input ~ 0
RXDO
Text GLabel 5800 1950 0    50   Input ~ 0
TXDO
Text GLabel 5800 1850 0    50   Input ~ 0
VDD
Text GLabel 5800 2150 0    50   Input ~ 0
GND
$Comp
L Device:R R1
U 1 1 5F21F8CD
P 1550 1550
F 0 "R1" H 1480 1504 50  0000 R CNN
F 1 "R" H 1480 1595 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 1480 1550 50  0001 C CNN
F 3 "~" H 1550 1550 50  0001 C CNN
	1    1550 1550
	-1   0    0    1   
$EndComp
Wire Wire Line
	1550 1400 1450 1400
Wire Wire Line
	1550 1700 1550 1900
Wire Wire Line
	1550 1900 1700 1900
Connection ~ 1550 1900
Text GLabel 1150 1900 0    50   Input ~ 0
GND
Wire Wire Line
	3200 2600 2900 2600
Text GLabel 3800 2600 2    50   Input ~ 0
GND
$EndSCHEMATC
