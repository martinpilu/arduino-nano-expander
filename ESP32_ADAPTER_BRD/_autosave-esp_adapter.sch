EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L RF_Module:ESP32-WROOM-32 U1
U 1 1 5F24C1DB
P 4500 3600
F 0 "U1" H 4500 5181 50  0000 C CNN
F 1 "ESP32-WROOM-32" H 4500 5090 50  0000 C CNN
F 2 "RF_Module:ESP32-WROOM-32" H 4500 2100 50  0001 C CNN
F 3 "https://www.espressif.com/sites/default/files/documentation/esp32-wroom-32_datasheet_en.pdf" H 4200 3650 50  0001 C CNN
	1    4500 3600
	1    0    0    -1  
$EndComp
Text GLabel 4500 2200 1    50   Input ~ 0
VDD
Text GLabel 4500 5000 3    50   Input ~ 0
GND
Text GLabel 6900 3850 0    50   Input ~ 0
VDD
Text GLabel 6900 5050 0    50   Input ~ 0
GND
Text GLabel 6900 4050 0    50   Input ~ 0
A0
Text GLabel 6900 4150 0    50   Input ~ 0
A1
Text GLabel 6900 4250 0    50   Input ~ 0
A2
Text GLabel 6900 4350 0    50   Input ~ 0
A3
Text GLabel 6900 4550 0    50   Input ~ 0
A5
Text GLabel 6900 4650 0    50   Input ~ 0
A6
Text GLabel 7850 5150 2    50   Input ~ 0
TX
Text GLabel 7850 5050 2    50   Input ~ 0
RX
Text GLabel 7850 4950 2    50   Input ~ 0
RST
Text GLabel 7850 4850 2    50   Input ~ 0
GND
Text GLabel 7850 4750 2    50   Input ~ 0
D2
Text GLabel 7850 4650 2    50   Input ~ 0
D3
Text GLabel 7850 4550 2    50   Input ~ 0
D4
Text GLabel 7850 4450 2    50   Input ~ 0
D5
Text GLabel 7850 4350 2    50   Input ~ 0
D6
Text GLabel 7850 4250 2    50   Input ~ 0
D7
Text GLabel 7850 4150 2    50   Input ~ 0
D8
Text GLabel 7850 4050 2    50   Input ~ 0
D9
Text GLabel 7850 3950 2    50   Input ~ 0
D10
Text GLabel 7850 3850 2    50   Input ~ 0
D11
Text GLabel 5100 2400 2    50   Input ~ 0
A1
Text GLabel 5100 3400 2    50   Input ~ 0
RX
Text GLabel 5100 3500 2    50   Input ~ 0
TX
Text GLabel 5100 2600 2    50   Input ~ 0
A2
Text GLabel 5100 3300 2    50   Input ~ 0
A3
Text GLabel 5100 2800 2    50   Input ~ 0
A0
Text GLabel 3900 2600 0    50   Input ~ 0
A6
Text GLabel 3900 2700 0    50   Input ~ 0
A7
$Comp
L Connector:Conn_01x15_Male J2
U 1 1 5F2A4E46
P 7650 4450
F 0 "J2" H 7758 5331 50  0000 C CNN
F 1 "Conn_01x15_Male" H 7758 5240 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x15_P2.54mm_Vertical" H 7650 4450 50  0001 C CNN
F 3 "~" H 7650 4450 50  0001 C CNN
	1    7650 4450
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x15_Male J1
U 1 1 5F2A6A58
P 7100 4450
F 0 "J1" H 7072 4474 50  0000 R CNN
F 1 "Conn_01x15_Male" H 7072 4383 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x15_P2.54mm_Vertical" H 7100 4450 50  0001 C CNN
F 3 "~" H 7100 4450 50  0001 C CNN
	1    7100 4450
	-1   0    0    -1  
$EndComp
Text GLabel 6900 4750 0    50   Input ~ 0
A7
Text GLabel 7850 3750 2    50   Input ~ 0
D12
Text GLabel 6900 4450 0    50   Input ~ 0
A4
Text GLabel 5100 4200 2    50   Input ~ 0
D2
Text GLabel 5100 4100 2    50   Input ~ 0
D7
Text GLabel 3650 1900 0    50   Input ~ 0
VDD
Text GLabel 5100 4400 2    50   Input ~ 0
A4
Text GLabel 5100 4500 2    50   Input ~ 0
A5
Text GLabel 5100 3000 2    50   Input ~ 0
D3
Text GLabel 5100 3600 2    50   Input ~ 0
D5
Text GLabel 5100 3200 2    50   Input ~ 0
D6
Text GLabel 5100 2900 2    50   Input ~ 0
D9
Text GLabel 3900 3700 0    50   Input ~ 0
D4
Text GLabel 5100 3700 2    50   Input ~ 0
D10
Text GLabel 5100 4300 2    50   Input ~ 0
D8
Text GLabel 6900 3750 0    50   Input ~ 0
D13
Text GLabel 5100 4000 2    50   Input ~ 0
D13
Text GLabel 5100 3800 2    50   Input ~ 0
D11
Text GLabel 5100 3900 2    50   Input ~ 0
D12
$Comp
L Switch:SW_Push SW1
U 1 1 5F21BD44
P 3550 2400
F 0 "SW1" H 3550 2685 50  0000 C CNN
F 1 "SW_Push" H 3550 2594 50  0000 C CNN
F 2 "Button_Switch_SMD:SW_SPST_CK_RS282G05A3" H 3550 2600 50  0001 C CNN
F 3 "~" H 3550 2600 50  0001 C CNN
	1    3550 2400
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x04_Female J3
U 1 1 5F21C2B7
P 8200 2450
F 0 "J3" H 8228 2426 50  0000 L CNN
F 1 "Conn_01x04_Female" H 8228 2335 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 8200 2450 50  0001 C CNN
F 3 "~" H 8200 2450 50  0001 C CNN
	1    8200 2450
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D1
U 1 1 5F21C7A0
P 5550 3100
F 0 "D1" H 5543 3317 50  0000 C CNN
F 1 "LED" H 5543 3226 50  0000 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 5550 3100 50  0001 C CNN
F 3 "~" H 5550 3100 50  0001 C CNN
	1    5550 3100
	-1   0    0    1   
$EndComp
$Comp
L Device:R R2
U 1 1 5F21CB7B
P 5850 3100
F 0 "R2" V 5643 3100 50  0000 C CNN
F 1 "R" V 5734 3100 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5780 3100 50  0001 C CNN
F 3 "~" H 5850 3100 50  0001 C CNN
	1    5850 3100
	0    1    1    0   
$EndComp
Text GLabel 5100 2500 2    50   Input ~ 0
TXDO
Text GLabel 5100 2700 2    50   Input ~ 0
RXDO
Text GLabel 8000 2550 0    50   Input ~ 0
RXDO
Text GLabel 8000 2450 0    50   Input ~ 0
TXDO
Text GLabel 8000 2350 0    50   Input ~ 0
VDD
Text GLabel 8000 2650 0    50   Input ~ 0
GND
$Comp
L Device:R R1
U 1 1 5F21F8CD
P 3750 2050
F 0 "R1" H 3680 2004 50  0000 R CNN
F 1 "R" H 3680 2095 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3680 2050 50  0001 C CNN
F 3 "~" H 3750 2050 50  0001 C CNN
	1    3750 2050
	-1   0    0    1   
$EndComp
Wire Wire Line
	3750 1900 3650 1900
Wire Wire Line
	3750 2200 3750 2400
Wire Wire Line
	3750 2400 3900 2400
Connection ~ 3750 2400
Text GLabel 3350 2400 0    50   Input ~ 0
GND
Wire Wire Line
	5400 3100 5100 3100
Text GLabel 6000 3100 2    50   Input ~ 0
GND
$EndSCHEMATC
